﻿using EG.EG_Models;
using EG.Windows;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Markup;
using System.IO;
using Microsoft.VisualBasic;
using System.Collections.ObjectModel;
using System.Text.Encodings.Web;
using ClosedXML.Excel;




namespace EG
    {
    public partial class MainWindow : Window
        {
        ApplicationContext db = new ApplicationContext();
        public MainWindow()
            {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
            }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
            {
            db.Database.EnsureCreated();
            db.Workers.Load();
            DataContext = db.Workers.Local.ToObservableCollection();
            }
        private void Create(object sender, RoutedEventArgs e)
            {
            WorkerWindow WorkerWindow = new WorkerWindow(new Worker());
            if (WorkerWindow.ShowDialog() == true)
                {
                Worker Worker = WorkerWindow.Worker;
                db.Workers.Add(Worker);
                db.SaveChanges();
                }
            }
        private void Hernya(object sender, RoutedEventArgs e)
            {
            Worker? Worker = usersList.SelectedItem as Worker;
            if (Worker is null) return;

            WorkerWindow WorkerWindow = new WorkerWindow(new Worker
                {
                Id = Worker.Id,
                Identificator = Worker.Identificator,
                SecondName = Worker.SecondName,
                FirstName = Worker.FirstName,
                Patronymic = Worker.Patronymic,
                BirthDay = Worker.BirthDay,
                Phone = Worker.Phone,
                Department = Worker.Department,

                });

            if (WorkerWindow.ShowDialog() == true)
                {
                Worker = db.Workers.Find(WorkerWindow.Worker.Id);
                if (Worker != null)
                    {
                    Worker.Identificator = WorkerWindow.Worker.Identificator;
                    Worker.SecondName = WorkerWindow.Worker.SecondName;
                    Worker.FirstName = WorkerWindow.Worker.FirstName;
                    Worker.Patronymic = WorkerWindow.Worker.Patronymic;
                    Worker.BirthDay = WorkerWindow.Worker.BirthDay;
                    Worker.Phone = WorkerWindow.Worker.Phone;
                    Worker.Department = WorkerWindow.Worker.Department;

                    db.SaveChanges();
                    usersList.Items.Refresh();
                    }
                }
            }
        private void Ubivat(object sender, RoutedEventArgs e)
            {
            Worker? Worker = usersList.SelectedItem as Worker;
            if (Worker is null) return;
            db.Workers.Remove(Worker);
            db.SaveChanges();
            }
        async private void Degenerate(object sender, RoutedEventArgs e)
            {
            db.Workers.Load();
            ObservableCollection<Worker> _data = (db.Workers.Local.ToObservableCollection());
            JsonSerializerOptions options = new JsonSerializerOptions()
                {
                Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create(System.Text.Unicode.UnicodeRanges.All)
                };
            try
                {
                string json = JsonSerializer.Serialize(_data, options);
                await using FileStream createStream = File.Create(@"..\..\..\..\Papka\J.Jhonna.json");
                await JsonSerializer.SerializeAsync(createStream, _data);
                MessageBox.Show(@"Дело сделанно");
                }
            catch (Exception oshibka)
                {
                MessageBox.Show(oshibka.Message);
                }
        }

        private void exelliarmus(object sender, EventArgs e)
        {
            try
            {
                string message = "This application has requested the Runtime to terminate it in an unusual way." +
                    " Please contact the application's support team for more information";
                string title = "Microsoft Visual Studio";
                MessageBox.Show(message, title);
            }
            catch (Exception oshibka)
            {
                MessageBox.Show(oshibka.Message);
            }
        }
        private void Button_Click()
            {

            }

        private void usersList_SelectionChanged()
            {

            }
        }
    }
