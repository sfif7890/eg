using EG.EG_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EG.Windows
{

    public partial class WorkerWindow : Window
    {
        public Worker Worker { get; private set; }
        public WorkerWindow(Worker worker)
        {
            InitializeComponent();
            Worker = worker;
            DataContext = Worker;
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
