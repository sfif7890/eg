﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EG.EG_Models
{
    public class Worker
    {
        public int Id { get; set; }
        public string? Identificator { get; set; }
        public string? SecondName { get; set; }
        public string? FirstName { get; set; }
        public string? Patronymic { get; set; }
        public string? BirthDay { get; set; }
        public string? Phone { get; set; }
        public string? Department { get; set; }

    }
}
